package main

import (
	"bufio"
	"fmt"
	"gitlab.com/orgeozerass/tictactoe/internal/entities"
	"gitlab.com/orgeozerass/tictactoe/internal/konst"
	"gitlab.com/orgeozerass/tictactoe/internal/repositories"
	"gitlab.com/orgeozerass/tictactoe/internal/usecases"
	"gitlab.com/orgeozerass/tictactoe/internal/utils"
	"strconv"
	"strings"

	gamerepo"gitlab.com/orgeozerass/tictactoe/internal/repositories/game"
	gameusecase "gitlab.com/orgeozerass/tictactoe/internal/usecases/game"
	"os"
)

var gameRepo repositories.GameRepositories
var gameUC usecases.GameUsecase
var reader *bufio.Reader

func readTile() (uint, error) {
	tileArg, tileErr := reader.ReadString('\n')
	utils.IsExit(tileArg)
	if tileErr != nil {
		fmt.Println(tileErr)
	}
	tileArg = strings.TrimSuffix(tileArg, "\n")
	tileNumber, tileConvErr := strconv.Atoi(tileArg)
	if tileConvErr != nil {
		return 0, tileConvErr
	}
	return uint(tileNumber), nil
}

func firstPlay(player entities.PlayerSide) error {
	if player == konst.PlayerXSide {
		return nil
	}

	fmt.Printf("\ncomputer turn")
	comPlayResult, comPlayErr := gameUC.ComputerPlayGame()
	if comPlayErr != nil {
		return comPlayErr
	}
	
	utils.PrintBoard(comPlayResult.GameData)
	return nil
}

func printErr(args ...interface{}) {
	fmt.Println("####", args)
}


func playGame(size uint) error {
	promptMessage("\ntype your tile number to play game (I am not fault tolerance then please type only available tile number)")
	tileNumber, tileNumberErr := readTile()
	if tileNumberErr != nil {
		printErr(tileNumberErr)
		playGame(size)
	} else if tileNumber < 1 || tileNumber > size * size {
		printErr("invalid tile number")
		playGame(size)
	}

	gameResult, playGameErr := gameUC.PlayerPlayGame(tileNumber)
	if playGameErr != nil {
		printErr(playGameErr)
		return playGameErr
	}

	utils.PrintBoard(gameResult.GameData)

	if *gameResult.IsGameEnd && gameResult.GameWinner != nil {
		fmt.Println("the winner is ", *gameResult.GameWinner)
		return nil
	}

	if *gameResult.IsGameEnd && gameResult.GameWinner == nil {
		fmt.Println("game end")
		return nil
	}

	fmt.Println("\ncomputer turn")
	comPlayResult, comPlayErr := gameUC.ComputerPlayGame()
	if comPlayErr != nil {
		printErr(comPlayErr)
		return comPlayErr
	}

	utils.PrintBoard(comPlayResult.GameData)

	if *comPlayResult.IsGameEnd && comPlayResult.GameWinner != nil {
		fmt.Println("the winner is ", *comPlayResult.GameWinner)
		return nil
	}

	if *comPlayResult.IsGameEnd && comPlayResult.GameWinner == nil {
		fmt.Println("game end")
		return nil
	}
	return playGame(size)
}

func promptMessage(msg string) {
	fmt.Println(msg)
	fmt.Print("$ ")
}

func main() {
	gameRepo = gamerepo.NewGameRepository()
	gameUC = gameusecase.NewGameUsecase(gameRepo)

	reader = bufio.NewReader(os.Stdin)
	for {
		fmt.Printf("\n-----------------------------------------")
		fmt.Printf("\nStart playing tic tac toe or you can type exit any time to leave this game")
		promptMessage("\n\nplease type X to play first or O to play later")
		player, playerErr := reader.ReadString('\n')
		if playerErr != nil {
			printErr(playerErr)
		}
		player = strings.TrimSuffix(player, "\n")
		utils.IsExit(player)
		player = strings.ToUpper(player)

		playerSide := entities.PlayerSide(player)
		if playerSide != konst.PlayerXSide && playerSide != konst.PlayerOSide {
			printErr("invalid player selected")
			continue
		}

		promptMessage("\nplease type some number to board size")
		sizeArg, sizeErr := reader.ReadString('\n')
		if sizeErr != nil {
			printErr(sizeErr)
		}
		sizeArg = strings.TrimSuffix(sizeArg, "\n")
		utils.IsExit(sizeArg)
		size, sizeConvErr := strconv.Atoi(sizeArg)
		if sizeConvErr != nil {
			printErr("your input size is invalid")
			continue
		}

		if size < 3 {
			printErr("your input size should be greater than 3")
			continue
		}

		gameData, startGameErr := gameUC.StartGame(entities.GameStartParam{
			GameSize:   uint(size),
			PlayerSide: playerSide,
		})

		if startGameErr != nil {
			printErr(startGameErr)
			continue
		}

		utils.PrintBoard(gameData)
		if play1stErr := firstPlay(entities.PlayerSide(player)); play1stErr != nil {
			printErr(play1stErr)
			continue
		}
		playGame(uint(size))
	}
}
