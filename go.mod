module gitlab.com/orgeozerass/tictactoe

go 1.15

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-playground/validator/v10 v10.6.1
	github.com/kr/pretty v0.1.0 // indirect
	github.com/stretchr/objx v0.1.1 // indirect
	github.com/stretchr/testify v1.7.0
	golang.org/x/sys v0.0.0-20190624142023-c5567b49c5d0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
