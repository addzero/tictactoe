This project is a TIC-TAC-TOE command line game written in Golang which.
The code design of the project will be implement according to Clean Architecture Pattern.

To start the program just run

```sh
go run cmd/main.go
```

and then follow the command instruction
