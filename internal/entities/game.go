package entities

type PlayerSide string

type GameData [][]PlayerSide

type PlayerGameScore struct {
	Rows     map[uint]uint
	Columns  map[uint]uint
	Diagonal map[uint]uint
}

type GameScore struct {
	PlayerX PlayerGameScore
	PlayerO PlayerGameScore
}

type GameStartParam struct {
	GameSize   uint
	PlayerSide PlayerSide
}

type PlayGameResult struct {
	GameData   GameData
	GameScore  *GameScore
	GameWinner *PlayerSide
	IsGameEnd  *bool
}
