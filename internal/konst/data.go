package konst

import "gitlab.com/orgeozerass/tictactoe/internal/entities"

var PlayerXSide entities.PlayerSide = "X"
var PlayerOSide entities.PlayerSide = "O"
