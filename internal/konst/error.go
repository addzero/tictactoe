package konst

import "fmt"

var ErrorTileIsNotAvailable = fmt.Errorf("select tile is not available")
