package utils

import (
	"fmt"
	"gitlab.com/orgeozerass/tictactoe/internal/entities"
	"os"
)

func GetTileRowColumn(tileNumber uint, gameData entities.GameData) (uint, uint) {
	column := (tileNumber - 1) % uint(len(gameData))
	row := (tileNumber - column - 1) / uint(len(gameData))
	return row, column
}

func IsExit(command string) {
	if command == "exit" {
		os.Exit(1)
	}
}

func PrintBoard(game entities.GameData) {
	fmt.Printf("\n---------------------------\n")
	for _, row := range game {
		for _, col := range row {
			fmt.Printf("%3s", col)
		}
		fmt.Println()
	}
	fmt.Println("---------------------------")
}
