package utils

import (
	"gitlab.com/orgeozerass/tictactoe/internal/entities"
	"testing"
)

func Test_getTileRowColumn(t *testing.T) {
	type args struct {
		tileNumber uint
		gameData   entities.GameData
	}
	tests := []struct {
		name       string
		args       args
		wantRow    uint
		wantColumn uint
	}{
		{
			name: "should return row 0 column 0 for tile 1",
			args: args{
				tileNumber: 1,
				gameData: entities.GameData{
					{"X", "O", "X"},
					{"X", "O", "X"},
					{"X", "O", "X"},
				},
			},
			wantRow:    0,
			wantColumn: 0,
		},
		{
			name: "should return row 0 column 1  for tile 2",
			args: args{
				tileNumber: 2,
				gameData: entities.GameData{
					{"X", "O", "X"},
					{"X", "O", "X"},
					{"X", "O", "X"},
				},
			},
			wantRow:    0,
			wantColumn: 1,
		},
		{
			name: "should return row 0 column 2  for tile 3",
			args: args{
				tileNumber: 3,
				gameData: entities.GameData{
					{"X", "O", "X"},
					{"X", "O", "X"},
					{"X", "O", "X"},
				},
			},
			wantRow:    0,
			wantColumn: 2,
		},
		{
			name: "should return row 1 column 0 for tile 4",
			args: args{
				tileNumber: 4,
				gameData: entities.GameData{
					{"X", "O", "X"},
					{"X", "O", "X"},
					{"X", "O", "X"},
				},
			},
			wantRow:    1,
			wantColumn: 0,
		},
		{
			name: "should return row 1 column 1  for tile 5",
			args: args{
				tileNumber: 5,
				gameData: entities.GameData{
					{"X", "O", "X"},
					{"X", "O", "X"},
					{"X", "O", "X"},
				},
			},
			wantRow:    1,
			wantColumn: 1,
		},
		{
			name: "should return row 1 column 2  for tile 6",
			args: args{
				tileNumber: 6,
				gameData: entities.GameData{
					{"X", "O", "X"},
					{"X", "O", "X"},
					{"X", "O", "X"},
				},
			},
			wantRow:    1,
			wantColumn: 2,
		},
		{
			name: "should return row 1 column 2  for tile 7",
			args: args{
				tileNumber: 7,
				gameData: entities.GameData{
					{"X", "O", "X"},
					{"X", "O", "X"},
					{"X", "O", "X"},
				},
			},
			wantRow:    2,
			wantColumn: 0,
		},
		{
			name: "should return row 1 column 2  for tile 8",
			args: args{
				tileNumber: 8,
				gameData: entities.GameData{
					{"X", "O", "X"},
					{"X", "O", "X"},
					{"X", "O", "X"},
				},
			},
			wantRow:    2,
			wantColumn: 1,
		},
		{
			name: "should return row 1 column 2  for tile 9",
			args: args{
				tileNumber: 9,
				gameData: entities.GameData{
					{"X", "O", "X"},
					{"X", "O", "X"},
					{"X", "O", "X"},
				},
			},
			wantRow:    2,
			wantColumn: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotRow, gotColumn := GetTileRowColumn(tt.args.tileNumber, tt.args.gameData)
			if gotRow != tt.wantRow {
				t.Errorf("getTileRowColumn() gotRow = %v, wantRow %v", gotRow, tt.wantRow)
			}
			if gotColumn != tt.wantColumn {
				t.Errorf("getTileRowColumn() gotColumn = %v, wantColumn %v", gotColumn, tt.wantColumn)
			}
		})
	}
}
