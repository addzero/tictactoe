package usecases

import "gitlab.com/orgeozerass/tictactoe/internal/entities"

type GameUsecase interface {
	StartGame(param entities.GameStartParam) (entities.GameData, error)
	PlayerPlayGame(uint) (*entities.PlayGameResult, error)
	ComputerPlayGame() (*entities.PlayGameResult, error)
}
