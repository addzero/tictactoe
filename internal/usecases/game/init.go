package game

import (
	"gitlab.com/orgeozerass/tictactoe/internal/repositories"
	"gitlab.com/orgeozerass/tictactoe/internal/usecases"
)

type gameUsecase struct {
	gameRepository repositories.GameRepositories
}

func NewGameUsecase(gameRepo repositories.GameRepositories) usecases.GameUsecase {
	return &gameUsecase{
		gameRepository: gameRepo,
	}
}
