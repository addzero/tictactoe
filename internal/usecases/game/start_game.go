package game

import (
	"gitlab.com/orgeozerass/tictactoe/internal/entities"
	"gitlab.com/orgeozerass/tictactoe/internal/konst"
)

func (g *gameUsecase) StartGame(param entities.GameStartParam) (entities.GameData, error) {
	var playerPlayFirst bool
	if param.PlayerSide == konst.PlayerXSide {
		playerPlayFirst = true
	}
	setPlayFirstErr := g.gameRepository.SetIsPlayerPlayFirst(playerPlayFirst)
	if setPlayFirstErr != nil {
		return nil, setPlayFirstErr
	}

	return g.gameRepository.CreateNewGame(param)
}
