package game

import (
	"fmt"
	"gitlab.com/orgeozerass/tictactoe/internal/entities"
	"gitlab.com/orgeozerass/tictactoe/internal/konst"
	"math/rand"
)

func simplePickTile(availableTile []uint) uint {
	rand.Seed(42)
	return availableTile[rand.Intn(len(availableTile))]
}

func (g *gameUsecase) ComputerPlayGame() (*entities.PlayGameResult, error) {
	playerSide, playerSideErr := g.gameRepository.GetPlayerSide()
	if playerSideErr != nil {
		return nil, playerSideErr
	}

	var computerSide entities.PlayerSide
	switch *playerSide {
	case konst.PlayerXSide:
		computerSide = konst.PlayerOSide
	case konst.PlayerOSide:
		computerSide = konst.PlayerXSide
	default:
		return nil, fmt.Errorf("cannot get player side")
	}

	availableTile, availableTileErr := g.gameRepository.GetAvailable()
	if availableTileErr != nil {
		return nil, availableTileErr
	}

	selectTile := simplePickTile(availableTile)

	gameData, updateGameErr := g.gameRepository.UpdateGame(selectTile, computerSide)
	if updateGameErr != nil {
		return nil, updateGameErr
	}

	gameScore, gameScoreErr := g.gameRepository.GetGameScore()
	if gameScoreErr != nil {
		return nil, gameScoreErr
	}

	isEnded, winner, isGameEndErr := g.isGameEnd(selectTile, &computerSide, gameData, gameScore)
	if isGameEndErr != nil {
		return nil, isGameEndErr
	}

	return &entities.PlayGameResult{
		GameData:   gameData,
		GameScore:  gameScore,
		GameWinner: winner,
		IsGameEnd:  isEnded,
	}, nil
}
