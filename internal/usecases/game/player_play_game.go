package game

import (
	"fmt"
	"gitlab.com/orgeozerass/tictactoe/internal/utils"

	"gitlab.com/orgeozerass/tictactoe/internal/entities"
	"gitlab.com/orgeozerass/tictactoe/internal/konst"
)

func (g *gameUsecase) isGameEnd(tileNumber uint, playerSide *entities.PlayerSide,
	gameData entities.GameData, gameScore *entities.GameScore) (*bool, *entities.PlayerSide, error) {
	if gameData == nil {
		return nil, nil, fmt.Errorf("game cannot be nil")
	}

	var dataToValidateWinner entities.PlayerGameScore
	switch *playerSide {
	case konst.PlayerXSide:
		dataToValidateWinner = gameScore.PlayerX
	case konst.PlayerOSide:
		dataToValidateWinner = gameScore.PlayerO
	default:
		return nil, nil, fmt.Errorf("fail to get player side")
	}

	var gameEnd bool
	row, column := utils.GetTileRowColumn(tileNumber, gameData)
	gameSize := uint(len(gameData))
	if dataToValidateWinner.Rows[row] == gameSize ||
		dataToValidateWinner.Columns[column] == gameSize ||
		dataToValidateWinner.Diagonal[0] == gameSize ||
		dataToValidateWinner.Diagonal[1] == gameSize { // validate game end by winning
		gameEnd = true
		return &gameEnd, playerSide, nil
	}

	availableTiles, availableErr := g.gameRepository.GetAvailable()
	if availableErr != nil {
		return nil, nil, availableErr
	}

	if len(availableTiles) == 0 {
		gameEnd = true
		return &gameEnd, nil, nil // game end by tire
	}

	return &gameEnd, nil, nil // game not end
}

func (g *gameUsecase) PlayerPlayGame(i uint) (*entities.PlayGameResult, error) {
	isAvailable, isAvailableErr := g.gameRepository.IsAvailable(i)
	if isAvailableErr != nil {
		return nil, isAvailableErr
	}

	if !isAvailable {
		return nil, konst.ErrorTileIsNotAvailable
	}

	playerSide, playerSideErr := g.gameRepository.GetPlayerSide()
	if playerSideErr != nil {
		return nil, playerSideErr
	}

	gameData, updateGameErr := g.gameRepository.UpdateGame(i, *playerSide)
	if updateGameErr != nil {
		return nil, updateGameErr
	}

	gameScore, gameScoreErr := g.gameRepository.GetGameScore()
	if gameScoreErr != nil {
		return nil, gameScoreErr
	}

	isEnded, winner, isGameEndErr := g.isGameEnd(i, playerSide, gameData, gameScore)
	if isGameEndErr != nil {
		return nil, isGameEndErr
	}

	return &entities.PlayGameResult{
		GameData:   gameData,
		GameScore:  gameScore,
		GameWinner: winner,
		IsGameEnd:  isEnded,
	}, nil
}
