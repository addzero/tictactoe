package game

import (
	"fmt"
	"gitlab.com/orgeozerass/tictactoe/internal/konst"
	"reflect"
	"testing"

	"github.com/stretchr/testify/mock"
	"gitlab.com/orgeozerass/tictactoe/internal/entities"
	"gitlab.com/orgeozerass/tictactoe/internal/repositories/mocks"
)

func Test_gameUsecase_PlayGame(t *testing.T) {
	gameEnd := true
	gameNotEnd := false
	type args struct {
		i uint
	}
	type mocker struct {
		gameRepoIsAvailableResult    bool
		gameRepoIsAvailableError     error
		gameRepoPlayerSideResult     *entities.PlayerSide
		gameRepoPlayerSideError      error
		gameRepoUpdateGameDataResult entities.GameData
		gameRepoUpdateGameDataError  error
		gameRepoGetAvailableResult   []uint
		gameRepoGetAvailableError    error
		gameRepoGetGameScoreResult   *entities.GameScore
		gameRepoGetGameScoreError    error
	}
	tests := []struct {
		name       string
		mocker     mocker
		args       args
		wantResult *entities.PlayGameResult
		wantErr    bool
	}{
		{
			name: "should got expect game data, game score with game not end and winner is nil if" +
				" gameRepository IsAvailable is return true" +
				" mameRepository PlayerSide not error" +
				" gameRepository UpdateGameData still remain some tile",
			args: args{i: 8},
			mocker: mocker{
				gameRepoIsAvailableResult: true,
				gameRepoPlayerSideResult:  &konst.PlayerXSide,
				gameRepoUpdateGameDataResult: [][]entities.PlayerSide{
					{"X", "O", "X"},
					{"X", "O", "O"},
					{"O", "X", "9"},
				},
				gameRepoGetGameScoreResult: &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:    map[uint]uint{0: 2, 1: 1, 2: 1},
						Columns: map[uint]uint{0: 2, 1: 1, 2: 1},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:    map[uint]uint{0: 1, 1: 2, 2:1},
						Columns: map[uint]uint{0: 1, 1: 2, 2: 1},
					},
				},
				gameRepoGetAvailableResult: []uint{8, 9},
			},
			wantResult: &entities.PlayGameResult{
				GameData: [][]entities.PlayerSide{
					{"X", "O", "X"},
					{"X", "O", "O"},
					{"O", "X", "9"},
				},
				GameScore: &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:    map[uint]uint{0: 2, 1: 1, 2: 1},
						Columns: map[uint]uint{0: 2, 1: 1, 2: 1},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:    map[uint]uint{0: 1, 1: 2, 2:1},
						Columns: map[uint]uint{0: 1, 1: 2, 2: 1},
					},
				},
				GameWinner: nil,
				IsGameEnd:  &gameNotEnd,
			},
			wantErr: false,
		},
		{
			name: "should got expect game data, game score and game end with winner if" +
				" gameRepository IsAvailable is return true" +
				" mameRepository PlayerSide not error" +
				" gameRepository UpdateGameData still remain some tile",
			args: args{i: 8},
			mocker: mocker{
				gameRepoIsAvailableResult: true,
				gameRepoPlayerSideResult:  &konst.PlayerXSide,
				gameRepoUpdateGameDataResult: [][]entities.PlayerSide{
					{"X", "O", "X"},
					{"O", "O", "6"},
					{"X", "X", "X"},
				},
				gameRepoGetGameScoreResult: &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:    map[uint]uint{0: 2, 1: 1, 2: 1},
						Columns: map[uint]uint{0: 2, 1: 1, 2: 1},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:    map[uint]uint{0: 1, 1: 2, 2:1},
						Columns: map[uint]uint{0: 1, 1: 2, 2: 1},
					},
				},
				gameRepoGetAvailableResult: []uint{6},
			},
			wantResult: &entities.PlayGameResult{
				GameData: [][]entities.PlayerSide{
					{"X", "O", "X"},
					{"O", "O", "6"},
					{"X", "X", "X"},
				},
				GameScore: &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:    map[uint]uint{0: 2, 1: 1, 2: 1},
						Columns: map[uint]uint{0: 2, 1: 1, 2: 1},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:    map[uint]uint{0: 1, 1: 2, 2:1},
						Columns: map[uint]uint{0: 1, 1: 2, 2: 1},
					},
				},
				GameWinner: nil,
				IsGameEnd:  &gameNotEnd,
			},
			wantErr: false,
		},
		{
			name: "should fails if" +
				" gameRepository IsAvailable is return false" +
				" gameRepository GetPlayerSide not error" +
				" gameRepository UpdateGameData still remain some tile",
			mocker: mocker{
				gameRepoIsAvailableResult: false,
				gameRepoPlayerSideResult:  &konst.PlayerXSide,
				gameRepoUpdateGameDataResult: [][]entities.PlayerSide{
					{"X", "O", "X"},
					{"X", "O", "O"},
					{"O", "X", "9"},
				},
			},
			wantResult: nil,
			wantErr:    true,
		},
		{
			name: "should fails if" +
				" gameRepository IsAvailable is return true" +
				" gameRepository PlayerSide return error" +
				" gameRepository UpdateGameData still remain some tile",
			mocker: mocker{
				gameRepoIsAvailableResult: true,
				gameRepoPlayerSideError:   fmt.Errorf("get player side error"),
				gameRepoUpdateGameDataResult: [][]entities.PlayerSide{
					{"X", "O", "X"},
					{"X", "O", "O"},
					{"O", "X", "9"},
				},
			},
			wantResult: nil,
			wantErr:       true,
		},
		{
			name: "should fails if" +
				" gameRepository IsAvailable is return true" +
				" gameRepository PlayerSide return X" +
				" gameRepository UpdateGameData return error",
			mocker: mocker{
				gameRepoIsAvailableResult:   true,
				gameRepoPlayerSideResult:    &konst.PlayerXSide,
				gameRepoUpdateGameDataError: fmt.Errorf("update game data error"),
			},
			wantResult: nil,
			wantErr:       true,
		},
		{
			name: "should fails if" +
				" gameRepository IsAvailable is return true" +
				" gameRepository PlayerSide return X" +
				" gameRepository UpdateGameData return nil",
			mocker: mocker{
				gameRepoIsAvailableResult:    true,
				gameRepoPlayerSideResult:     &konst.PlayerXSide,
				gameRepoUpdateGameDataResult: nil,
			},
			wantResult: nil,
			wantErr:       true,
		},
		{
			name: "should fails if" +
				" gameRepository IsAvailable is return true" +
				" gameRepository PlayerSide return X" +
				" gameRepository UpdateGameData return data" +
				" gameRepository GetGameScore return error",
			mocker: mocker{
				gameRepoIsAvailableResult:    true,
				gameRepoPlayerSideResult:     &konst.PlayerXSide,
				gameRepoUpdateGameDataResult: [][]entities.PlayerSide{
					{"X", "O", "X"},
					{"X", "O", "O"},
					{"O", "X", "O"},
				},
				gameRepoGetGameScoreError: fmt.Errorf("GetGameScore error"),
			},
			wantResult:    nil,
			wantErr:       true,
		},
		{
			name: "should fails if" +
				" gameRepository IsAvailable is return true" +
				" gameRepository PlayerSide return X" +
				" gameRepository UpdateGameData return data" +
				" gameRepository GetGameScore return data" +
				" gameRepository GetAvailable return error",
			args: args{i: 8},
			mocker: mocker{
				gameRepoIsAvailableResult: true,
				gameRepoPlayerSideResult:  &konst.PlayerXSide,
				gameRepoUpdateGameDataResult: [][]entities.PlayerSide{
					{"X", "O", "X"},
					{"X", "O", "O"},
					{"O", "8", "9"},
				},
				gameRepoGetGameScoreResult: &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{},
						Columns:  map[uint]uint{},
						Diagonal: map[uint]uint{},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{},
						Columns:  map[uint]uint{},
						Diagonal: map[uint]uint{},
					},
				},
				gameRepoGetAvailableError: fmt.Errorf("GetAvailable error"),
			},
			wantResult: nil,
			wantErr:    true,
		},
		{
			name: "should got expect game data and isGameEnd if" +
				" gameRepository IsAvailable is return true" +
				" mameRepository PlayerSide return X" +
				" gameRepository UpdateGameData return data" +
				" gameRepository GetGameScore return data" +
				" gameRepository GetAvailable return data length as 0",
			mocker: mocker{
				gameRepoIsAvailableResult: true,
				gameRepoPlayerSideResult:  &konst.PlayerXSide,
				gameRepoUpdateGameDataResult: [][]entities.PlayerSide{
					{"X", "O", "X"},
					{"X", "O", "O"},
					{"O", "X", "O"},
				},
				gameRepoGetGameScoreResult: &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{},
						Columns:  map[uint]uint{},
						Diagonal: map[uint]uint{},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{},
						Columns:  map[uint]uint{},
						Diagonal: map[uint]uint{},
					},
				},
				gameRepoGetAvailableResult: nil,
			},
			wantResult: &entities.PlayGameResult{
				GameData:   [][]entities.PlayerSide{
					{"X", "O", "X"},
					{"X", "O", "O"},
					{"O", "X", "O"},
				},
				GameScore:  &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{},
						Columns:  map[uint]uint{},
						Diagonal: map[uint]uint{},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{},
						Columns:  map[uint]uint{},
						Diagonal: map[uint]uint{},
					},
				},
				GameWinner: nil,
				IsGameEnd:  &gameEnd,
			},
			wantErr: false,
		},
		{
			name: "should got game end with the winner if" +
				" gameRepository IsAvailable is return true" +
				" mameRepository PlayerSide return X" +
				" gameRepository UpdateGameData return data" +
				" gameRepository GetGameScore return return some data match the game length",
			args: args{i: 9},
			mocker: mocker{
				gameRepoIsAvailableResult: true,
				gameRepoPlayerSideResult:  &konst.PlayerXSide,
				gameRepoUpdateGameDataResult: [][]entities.PlayerSide{
					{"X", "O", "O"},
					{"O", "X", "O"},
					{"X", "8", "X"},
				},
				gameRepoGetGameScoreResult: &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 1, 1: 1, 2: 2},
						Columns:  map[uint]uint{0: 2, 1: 2, 2: 1},
						Diagonal: map[uint]uint{0: 3, 1: 2},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 2, 1: 2, 2: 0},
						Columns:  map[uint]uint{0: 1, 1: 1, 3: 2},
						Diagonal: map[uint]uint{0: 0, 1: 1},
					},
				},
				gameRepoGetAvailableResult: []uint{8},
			},
			wantResult: &entities.PlayGameResult{
				GameData:   [][]entities.PlayerSide{
					{"X", "O", "O"},
					{"O", "X", "O"},
					{"X", "8", "X"},
				},
				GameScore:  &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 1, 1: 1, 2: 2},
						Columns:  map[uint]uint{0: 2, 1: 2, 2: 1},
						Diagonal: map[uint]uint{0: 3, 1: 2},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 2, 1: 2, 2: 0},
						Columns:  map[uint]uint{0: 1, 1: 1, 3: 2},
						Diagonal: map[uint]uint{0: 0, 1: 1},
					},
				},
				GameWinner: &konst.PlayerXSide,
				IsGameEnd:  &gameEnd,
			},
			wantErr: false,
		},
		{
			name: "should got game end with the winner if" +
				" gameRepository IsAvailable is return true" +
				" mameRepository PlayerSide return X" +
				" gameRepository UpdateGameData return data" +
				" gameRepository GetGameScore return return score at diagonal 0 of player X match the game length",
			args: args{i: 9},
			mocker: mocker{
				gameRepoIsAvailableResult: true,
				gameRepoPlayerSideResult:  &konst.PlayerXSide,
				gameRepoUpdateGameDataResult: [][]entities.PlayerSide{
					{"X", "O", "O"},
					{"O", "X", "O"},
					{"X", "8", "X"},
				},
				gameRepoGetGameScoreResult: &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 1, 1: 1, 2: 2},
						Columns:  map[uint]uint{0: 2, 1: 2, 2: 1},
						Diagonal: map[uint]uint{0: 3, 1: 2},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 2, 1: 2, 2: 0},
						Columns:  map[uint]uint{0: 1, 1: 1, 3: 2},
						Diagonal: map[uint]uint{0: 0, 1: 1},
					},
				},
				gameRepoGetAvailableResult: []uint{8},
			},
			wantResult: &entities.PlayGameResult{
				GameData:   [][]entities.PlayerSide{
					{"X", "O", "O"},
					{"O", "X", "O"},
					{"X", "8", "X"},
				},
				GameScore:  &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 1, 1: 1, 2: 2},
						Columns:  map[uint]uint{0: 2, 1: 2, 2: 1},
						Diagonal: map[uint]uint{0: 3, 1: 2},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 2, 1: 2, 2: 0},
						Columns:  map[uint]uint{0: 1, 1: 1, 3: 2},
						Diagonal: map[uint]uint{0: 0, 1: 1},
					},
				},
				GameWinner: &konst.PlayerXSide,
				IsGameEnd:  &gameEnd,
			},
			wantErr: false,
		},
		{
			name: "should got game end with the winner if" +
				" gameRepository IsAvailable is return true" +
				" mameRepository PlayerSide return X" +
				" gameRepository UpdateGameData return data" +
				" gameRepository GetGameScore return return score at diagonal 1 of player X match the game length",
			args: args{i: 9},
			mocker: mocker{
				gameRepoIsAvailableResult: true,
				gameRepoPlayerSideResult:  &konst.PlayerXSide,
				gameRepoUpdateGameDataResult: [][]entities.PlayerSide{
					{"X", "O", "O"},
					{"O", "X", "O"},
					{"X", "8", "X"},
				},
				gameRepoGetGameScoreResult: &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 1, 1: 1, 2: 2},
						Columns:  map[uint]uint{0: 2, 1: 2, 2: 1},
						Diagonal: map[uint]uint{0: 0, 1: 3},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 2, 1: 2, 2: 0},
						Columns:  map[uint]uint{0: 1, 1: 1, 3: 2},
						Diagonal: map[uint]uint{0: 0, 1: 1},
					},
				},
				gameRepoGetAvailableResult: []uint{8},
			},
			wantResult: &entities.PlayGameResult{
				GameData:   [][]entities.PlayerSide{
					{"X", "O", "O"},
					{"O", "X", "O"},
					{"X", "8", "X"},
				},
				GameScore:  &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 1, 1: 1, 2: 2},
						Columns:  map[uint]uint{0: 2, 1: 2, 2: 1},
						Diagonal: map[uint]uint{0: 0, 1: 3},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 2, 1: 2, 2: 0},
						Columns:  map[uint]uint{0: 1, 1: 1, 3: 2},
						Diagonal: map[uint]uint{0: 0, 1: 1},
					},
				},
				GameWinner: &konst.PlayerXSide,
				IsGameEnd:  &gameEnd,
			},
			wantErr: false,
		},
		{
			name: "should got game end with the winner if" +
				" gameRepository IsAvailable is return true" +
				" mameRepository PlayerSide return X" +
				" gameRepository UpdateGameData return data" +
				" gameRepository GetGameScore return return score at row 0 of player X match the game length",
			args: args{i: 9},
			mocker: mocker{
				gameRepoIsAvailableResult: true,
				gameRepoPlayerSideResult:  &konst.PlayerXSide,
				gameRepoUpdateGameDataResult: [][]entities.PlayerSide{
					{"X", "O", "O"},
					{"O", "X", "O"},
					{"X", "8", "X"},
				},
				gameRepoGetGameScoreResult: &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 3, 1: 1, 2: 2},
						Columns:  map[uint]uint{0: 2, 1: 2, 2: 1},
						Diagonal: map[uint]uint{0: 0, 1: 3},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 2, 1: 2, 2: 0},
						Columns:  map[uint]uint{0: 1, 1: 1, 3: 2},
						Diagonal: map[uint]uint{0: 0, 1: 1},
					},
				},
				gameRepoGetAvailableResult: []uint{8},
			},
			wantResult: &entities.PlayGameResult{
				GameData:   [][]entities.PlayerSide{
					{"X", "O", "O"},
					{"O", "X", "O"},
					{"X", "8", "X"},
				},
				GameScore:  &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 3, 1: 1, 2: 2},
						Columns:  map[uint]uint{0: 2, 1: 2, 2: 1},
						Diagonal: map[uint]uint{0: 0, 1: 3},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 2, 1: 2, 2: 0},
						Columns:  map[uint]uint{0: 1, 1: 1, 3: 2},
						Diagonal: map[uint]uint{0: 0, 1: 1},
					},
				},
				GameWinner: &konst.PlayerXSide,
				IsGameEnd:  &gameEnd,
			},
			wantErr: false,
		},
		{
			name: "should got game end with the winner if" +
				" gameRepository IsAvailable is return true" +
				" mameRepository PlayerSide return X" +
				" gameRepository UpdateGameData return data" +
				" gameRepository GetGameScore return return score at row 1 of player X match the game length",
			args: args{i: 9},
			mocker: mocker{
				gameRepoIsAvailableResult: true,
				gameRepoPlayerSideResult:  &konst.PlayerXSide,
				gameRepoUpdateGameDataResult: [][]entities.PlayerSide{
					{"X", "O", "O"},
					{"O", "X", "O"},
					{"X", "8", "X"},
				},
				gameRepoGetGameScoreResult: &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 0, 1: 3, 2: 2},
						Columns:  map[uint]uint{0: 2, 1: 2, 2: 1},
						Diagonal: map[uint]uint{0: 0, 1: 3},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 2, 1: 2, 2: 0},
						Columns:  map[uint]uint{0: 1, 1: 1, 3: 2},
						Diagonal: map[uint]uint{0: 0, 1: 1},
					},
				},
				gameRepoGetAvailableResult: []uint{8},
			},
			wantResult: &entities.PlayGameResult{
				GameData:   [][]entities.PlayerSide{
					{"X", "O", "O"},
					{"O", "X", "O"},
					{"X", "8", "X"},
				},
				GameScore:  &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 0, 1: 3, 2: 2},
						Columns:  map[uint]uint{0: 2, 1: 2, 2: 1},
						Diagonal: map[uint]uint{0: 0, 1: 3},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 2, 1: 2, 2: 0},
						Columns:  map[uint]uint{0: 1, 1: 1, 3: 2},
						Diagonal: map[uint]uint{0: 0, 1: 1},
					},
				},
				GameWinner: &konst.PlayerXSide,
				IsGameEnd:  &gameEnd,
			},
			wantErr: false,
		},
		{
			name: "should got game end with the winner if" +
				" gameRepository IsAvailable is return true" +
				" mameRepository PlayerSide return X" +
				" gameRepository UpdateGameData return data" +
				" gameRepository GetGameScore return return score at row 3 of player X match the game length",
			args: args{i: 9},
			mocker: mocker{
				gameRepoIsAvailableResult: true,
				gameRepoPlayerSideResult:  &konst.PlayerXSide,
				gameRepoUpdateGameDataResult: [][]entities.PlayerSide{
					{"X", "O", "O"},
					{"O", "X", "O"},
					{"X", "8", "X"},
				},
				gameRepoGetGameScoreResult: &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 0, 1: 2, 2: 3},
						Columns:  map[uint]uint{0: 2, 1: 2, 2: 1},
						Diagonal: map[uint]uint{0: 0, 1: 3},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 2, 1: 2, 2: 0},
						Columns:  map[uint]uint{0: 1, 1: 1, 3: 2},
						Diagonal: map[uint]uint{0: 0, 1: 1},
					},
				},
				gameRepoGetAvailableResult: []uint{8},
			},
			wantResult: &entities.PlayGameResult{
				GameData:   [][]entities.PlayerSide{
					{"X", "O", "O"},
					{"O", "X", "O"},
					{"X", "8", "X"},
				},
				GameScore:  &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 0, 1: 2, 2: 3},
						Columns:  map[uint]uint{0: 2, 1: 2, 2: 1},
						Diagonal: map[uint]uint{0: 0, 1: 3},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 2, 1: 2, 2: 0},
						Columns:  map[uint]uint{0: 1, 1: 1, 3: 2},
						Diagonal: map[uint]uint{0: 0, 1: 1},
					},
				},
				GameWinner: &konst.PlayerXSide,
				IsGameEnd:  &gameEnd,
			},
			wantErr: false,
		},

		{
			name: "should got game end with the winner if" +
				" gameRepository IsAvailable is return true" +
				" mameRepository PlayerSide return X" +
				" gameRepository UpdateGameData return data" +
				" gameRepository GetGameScore return return score at column 0 of player X match the game length",
			args: args{i: 9},
			mocker: mocker{
				gameRepoIsAvailableResult: true,
				gameRepoPlayerSideResult:  &konst.PlayerXSide,
				gameRepoUpdateGameDataResult: [][]entities.PlayerSide{
					{"X", "O", "O"},
					{"O", "X", "O"},
					{"X", "8", "X"},
				},
				gameRepoGetGameScoreResult: &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 2, 1: 1, 2: 2},
						Columns:  map[uint]uint{0: 3, 1: 2, 2: 1},
						Diagonal: map[uint]uint{0: 0, 1: 3},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 2, 1: 2, 2: 0},
						Columns:  map[uint]uint{0: 1, 1: 1, 3: 2},
						Diagonal: map[uint]uint{0: 0, 1: 1},
					},
				},
				gameRepoGetAvailableResult: []uint{8},
			},
			wantResult: &entities.PlayGameResult{
				GameData:   [][]entities.PlayerSide{
					{"X", "O", "O"},
					{"O", "X", "O"},
					{"X", "8", "X"},
				},
				GameScore:  &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 2, 1: 1, 2: 2},
						Columns:  map[uint]uint{0: 3, 1: 2, 2: 1},
						Diagonal: map[uint]uint{0: 0, 1: 3},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 2, 1: 2, 2: 0},
						Columns:  map[uint]uint{0: 1, 1: 1, 3: 2},
						Diagonal: map[uint]uint{0: 0, 1: 1},
					},
				},
				GameWinner: &konst.PlayerXSide,
				IsGameEnd:  &gameEnd,
			},
			wantErr: false,
		},
		{
			name: "should got game end with the winner if" +
				" gameRepository IsAvailable is return true" +
				" mameRepository PlayerSide return X" +
				" gameRepository UpdateGameData return data" +
				" gameRepository GetGameScore return return score at column 1 of player X match the game length",
			args: args{i: 9},
			mocker: mocker{
				gameRepoIsAvailableResult: true,
				gameRepoPlayerSideResult:  &konst.PlayerXSide,
				gameRepoUpdateGameDataResult: [][]entities.PlayerSide{
					{"X", "O", "O"},
					{"O", "X", "O"},
					{"X", "8", "X"},
				},
				gameRepoGetGameScoreResult: &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 0, 1: 2, 2: 2},
						Columns:  map[uint]uint{0: 2, 1: 3, 2: 1},
						Diagonal: map[uint]uint{0: 0, 1: 3},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 2, 1: 2, 2: 0},
						Columns:  map[uint]uint{0: 1, 1: 1, 3: 2},
						Diagonal: map[uint]uint{0: 0, 1: 1},
					},
				},
				gameRepoGetAvailableResult: []uint{8},
			},
			wantResult: &entities.PlayGameResult{
				GameData:   [][]entities.PlayerSide{
					{"X", "O", "O"},
					{"O", "X", "O"},
					{"X", "8", "X"},
				},
				GameScore:  &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 0, 1: 2, 2: 2},
						Columns:  map[uint]uint{0: 2, 1: 3, 2: 1},
						Diagonal: map[uint]uint{0: 0, 1: 3},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 2, 1: 2, 2: 0},
						Columns:  map[uint]uint{0: 1, 1: 1, 3: 2},
						Diagonal: map[uint]uint{0: 0, 1: 1},
					},
				},
				GameWinner: &konst.PlayerXSide,
				IsGameEnd:  &gameEnd,
			},
			wantErr: false,
		},
		{
			name: "should got game end with the winner if" +
				" gameRepository IsAvailable is return true" +
				" mameRepository PlayerSide return X" +
				" gameRepository UpdateGameData return data" +
				" gameRepository GetGameScore return return score at column 2 of player X match the game length",
			args: args{i: 9},
			mocker: mocker{
				gameRepoIsAvailableResult: true,
				gameRepoPlayerSideResult:  &konst.PlayerXSide,
				gameRepoUpdateGameDataResult: [][]entities.PlayerSide{
					{"X", "O", "O"},
					{"O", "X", "O"},
					{"X", "8", "X"},
				},
				gameRepoGetGameScoreResult: &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 0, 1: 2, 2: 2},
						Columns:  map[uint]uint{0: 2, 1: 2, 2: 3},
						Diagonal: map[uint]uint{0: 0, 1: 3},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 2, 1: 2, 2: 0},
						Columns:  map[uint]uint{0: 1, 1: 1, 3: 2},
						Diagonal: map[uint]uint{0: 0, 1: 1},
					},
				},
				gameRepoGetAvailableResult: []uint{8},
			},
			wantResult: &entities.PlayGameResult{
				GameData:   [][]entities.PlayerSide{
					{"X", "O", "O"},
					{"O", "X", "O"},
					{"X", "8", "X"},
				},
				GameScore:  &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 0, 1: 2, 2: 2},
						Columns:  map[uint]uint{0: 2, 1: 2, 2: 3},
						Diagonal: map[uint]uint{0: 0, 1: 3},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 2, 1: 2, 2: 0},
						Columns:  map[uint]uint{0: 1, 1: 1, 3: 2},
						Diagonal: map[uint]uint{0: 0, 1: 1},
					},
				},
				GameWinner: &konst.PlayerXSide,
				IsGameEnd:  &gameEnd,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gameRepo := new(mocks.GameRepositories)
			gameRepo.On("IsAvailable", mock.Anything).
				Return(tt.mocker.gameRepoIsAvailableResult, tt.mocker.gameRepoIsAvailableError)
			gameRepo.On("GetPlayerSide", mock.Anything).
				Return(tt.mocker.gameRepoPlayerSideResult, tt.mocker.gameRepoPlayerSideError)
			gameRepo.On("GetAvailable").
				Return(tt.mocker.gameRepoGetAvailableResult, tt.mocker.gameRepoGetAvailableError)
			gameRepo.On("UpdateGame", mock.Anything, mock.Anything).
				Return(tt.mocker.gameRepoUpdateGameDataResult, tt.mocker.gameRepoUpdateGameDataError)
			gameRepo.On("GetGameScore").
				Return(tt.mocker.gameRepoGetGameScoreResult, tt.mocker.gameRepoGetGameScoreError)
			g := NewGameUsecase(gameRepo)
			got, err := g.PlayerPlayGame(tt.args.i)
			if (err != nil) != tt.wantErr {
				t.Errorf("PlayGame() getError = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.wantResult) {
				t.Errorf("PlayGame() gotGameResult = %v, wantGameResult %v", got, tt.wantResult)
			}
		})
	}
}

//func Test_isGameEnd(t *testing.T) {
//	gameEnd := true
//	gameNotEnd := false
//	type args struct {
//		gameData entities.GameData
//	}
//	tests := []struct {
//		name    string
//		args    args
//		want    *bool
//		wantErr bool
//	}{
//		{
//			name:    "should return error if game data is nil",
//			args:    args{gameData: nil},
//			want:    nil,
//			wantErr: true,
//		},
//		{
//			name:    "should return game end if game size is 0",
//			args:    args{gameData: [][]entities.PlayerSide{}},
//			want:    &gameEnd,
//			wantErr: false,
//		},
//		{
//			name: "should return game end if game size is 1 with all tile marked",
//			args: args{gameData: [][]entities.PlayerSide{
//				{"X"},
//			}},
//			want:    &gameEnd,
//			wantErr: false,
//		},
//		{
//			name: "should return game not end if game size is 1 with some tile not yet marked",
//			args: args{gameData: [][]entities.PlayerSide{
//				{"1"},
//			}},
//			want:    &gameNotEnd,
//			wantErr: false,
//		},
//		{
//			name: "should return game end if game size is 3 with all tile marked",
//			args: args{gameData: [][]entities.PlayerSide{
//				{"X", "O", "X"}, {"O", "X", "O"}, {"X", "O", "X"},
//			}},
//			want:    &gameEnd,
//			wantErr: false,
//		},
//		{
//			name: "should return game not end if game size is 3 with some tile not yet marked",
//			args: args{gameData: [][]entities.PlayerSide{
//				{"X", "O", "X"}, {"O", "X", "O"}, {"X", "O", "9"},
//			}},
//			want:    &gameNotEnd,
//			wantErr: false,
//		},
//	}
//	for _, tt := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			got, err := isGameEnd(tt.args.gameData)
//			if (err != nil) != tt.wantErr {
//				t.Errorf("isGameEnd() error = %v, wantErr %v", err, tt.wantErr)
//				return
//			}
//			if !reflect.DeepEqual(got, tt.want) {
//				t.Errorf("isGameEnd() got = %v, want %v", got, tt.want)
//			}
//		})
//	}
//}
