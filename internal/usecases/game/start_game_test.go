package game

import (
	"fmt"
	"reflect"
	"testing"

	mock "github.com/stretchr/testify/mock"
	"gitlab.com/orgeozerass/tictactoe/internal/entities"
	"gitlab.com/orgeozerass/tictactoe/internal/konst"
	"gitlab.com/orgeozerass/tictactoe/internal/repositories"
	"gitlab.com/orgeozerass/tictactoe/internal/repositories/mocks"
)

func Test_gameUsecase_StartGame(t *testing.T) {
	type fields struct {
		gameRepository repositories.GameRepositories
	}
	type mocker struct {
		mockGameRepoSetIsPlayerPlayFirstError error
		mockGameRepoCreateGameGameData        entities.GameData
		mockGameRepoCreateGameError           error
	}
	type args struct {
		param entities.GameStartParam
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		mocker  mocker
		want    entities.GameData
		wantErr bool
	}{
		{
			name: "shoud got error if SetIsPlayerPlayFirst return error",
			args: args{param: entities.GameStartParam{GameSize: 3, PlayerSide: konst.PlayerXSide}},
			mocker: mocker{
				mockGameRepoSetIsPlayerPlayFirstError: fmt.Errorf("setIsPlayerPlayFirst error"),
				mockGameRepoCreateGameGameData: [][]entities.PlayerSide{
					{"1", "2", "3"},
					{"4", "5", "6"},
					{"7", "8", "9"},
				},
			},
			wantErr: true,
		},
		{
			name: "shoud got error if CreateNewGame return error",
			args: args{param: entities.GameStartParam{GameSize: 3, PlayerSide: konst.PlayerXSide}},
			mocker: mocker{
				mockGameRepoSetIsPlayerPlayFirstError: nil,
				mockGameRepoCreateGameGameData:        nil,
				mockGameRepoCreateGameError:           fmt.Errorf("createNewGame error"),
			},
			wantErr: true,
		},
		{
			name: "shoud got game data if createNewGame success with data",
			args: args{param: entities.GameStartParam{GameSize: 3, PlayerSide: konst.PlayerXSide}},
			mocker: mocker{
				mockGameRepoSetIsPlayerPlayFirstError: nil,
				mockGameRepoCreateGameGameData: [][]entities.PlayerSide{
					{"1", "2", "3"},
					{"4", "5", "6"},
					{"7", "8", "9"},
				},
				mockGameRepoCreateGameError: nil,
			},
			wantErr: false,
			want: [][]entities.PlayerSide{
				{"1", "2", "3"},
				{"4", "5", "6"},
				{"7", "8", "9"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gameRepo := new(mocks.GameRepositories)
			gameRepo.On("SetIsPlayerPlayFirst", mock.Anything).
				Return(tt.mocker.mockGameRepoSetIsPlayerPlayFirstError)
			gameRepo.On("CreateNewGame", mock.Anything).
				Return(tt.mocker.mockGameRepoCreateGameGameData, tt.mocker.mockGameRepoCreateGameError)
			g := NewGameUsecase(gameRepo)
			got, err := g.StartGame(tt.args.param)
			if (err != nil) != tt.wantErr {
				t.Errorf("StartGame() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("StartGame() got = %v, wantIsGameEnd %v", got, tt.want)
			}
		})
	}
}
