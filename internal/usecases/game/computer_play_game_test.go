package game

import (
	"fmt"
	"github.com/stretchr/testify/mock"
	"gitlab.com/orgeozerass/tictactoe/internal/entities"
	"gitlab.com/orgeozerass/tictactoe/internal/konst"
	"gitlab.com/orgeozerass/tictactoe/internal/repositories/mocks"
	"reflect"
	"testing"
)

func Test_gameUsecase_ComputerPlayGame(t *testing.T) {
	gameEnd := true
	gameNotEnd := false
	type mocker struct {
		gameRepoPlayerSideResult     *entities.PlayerSide
		gameRepoPlayerSideError      error
		gameRepoUpdateGameDataResult entities.GameData
		gameRepoUpdateGameDataError  error
		gameRepoGetAvailableResult   []uint
		gameRepoGetAvailableError    error
		gameRepoGetGameScoreResult   *entities.GameScore
		gameRepoGetGameScoreError    error
	}
	tests := []struct {
		name    string
		mocker  mocker
		wantResult    *entities.PlayGameResult
		wantErr bool
	}{
		{
			name: "should got expect game data, game score with game not end and winner is nil if" +
				" mameRepository PlayerSide not error" +
				" gameRepository UpdateGameData still remain some tile",
			mocker: mocker{
				gameRepoPlayerSideResult:  &konst.PlayerXSide,
				gameRepoUpdateGameDataResult: [][]entities.PlayerSide{
					{"X", "O", "X"},
					{"X", "O", "O"},
					{"O", "X", "9"},
				},
				gameRepoGetGameScoreResult: &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:    map[uint]uint{0: 2, 1: 1, 2: 1},
						Columns: map[uint]uint{0: 2, 1: 1, 2: 1},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:    map[uint]uint{0: 1, 1: 2, 2:1},
						Columns: map[uint]uint{0: 1, 1: 2, 2: 1},
					},
				},
				gameRepoGetAvailableResult: []uint{8, 9},
			},
			wantResult: &entities.PlayGameResult{
				GameData: [][]entities.PlayerSide{
					{"X", "O", "X"},
					{"X", "O", "O"},
					{"O", "X", "9"},
				},
				GameScore: &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:    map[uint]uint{0: 2, 1: 1, 2: 1},
						Columns: map[uint]uint{0: 2, 1: 1, 2: 1},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:    map[uint]uint{0: 1, 1: 2, 2:1},
						Columns: map[uint]uint{0: 1, 1: 2, 2: 1},
					},
				},
				GameWinner: nil,
				IsGameEnd:  &gameNotEnd,
			},
			wantErr: false,
		},
		{
			name: "should got expect game data, game score and game end with winner if" +
				" mameRepository PlayerSide not error" +
				" gameRepository UpdateGameData still remain some tile",
			mocker: mocker{
				gameRepoPlayerSideResult:  &konst.PlayerXSide,
				gameRepoUpdateGameDataResult: [][]entities.PlayerSide{
					{"X", "O", "X"},
					{"O", "O", "6"},
					{"X", "X", "X"},
				},
				gameRepoGetGameScoreResult: &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:    map[uint]uint{0: 2, 1: 1, 2: 1},
						Columns: map[uint]uint{0: 2, 1: 1, 2: 1},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:    map[uint]uint{0: 1, 1: 2, 2:1},
						Columns: map[uint]uint{0: 1, 1: 2, 2: 1},
					},
				},
				gameRepoGetAvailableResult: []uint{6},
			},
			wantResult: &entities.PlayGameResult{
				GameData: [][]entities.PlayerSide{
					{"X", "O", "X"},
					{"O", "O", "6"},
					{"X", "X", "X"},
				},
				GameScore: &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:    map[uint]uint{0: 2, 1: 1, 2: 1},
						Columns: map[uint]uint{0: 2, 1: 1, 2: 1},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:    map[uint]uint{0: 1, 1: 2, 2:1},
						Columns: map[uint]uint{0: 1, 1: 2, 2: 1},
					},
				},
				GameWinner: nil,
				IsGameEnd:  &gameNotEnd,
			},
			wantErr: false,
		},
		{
			name: "should fails if" +
				" gameRepository PlayerSide return error" +
				" gameRepository UpdateGameData still remain some tile",
			mocker: mocker{
				gameRepoPlayerSideError:   fmt.Errorf("get player side error"),
				gameRepoUpdateGameDataResult: [][]entities.PlayerSide{
					{"X", "O", "X"},
					{"X", "O", "O"},
					{"O", "X", "9"},
				},
			},
			wantResult: nil,
			wantErr:       true,
		},
		{
			name: "should fails if" +
				" gameRepository PlayerSide return X" +
				" gameRepository GetAvailable return data" +
				" gameRepository UpdateGameData return error",
			mocker: mocker{
				gameRepoPlayerSideResult:    &konst.PlayerXSide,
				gameRepoGetAvailableResult: []uint{1, 2, 3, 4, 5, 6, 7, 8, 9},
				gameRepoUpdateGameDataError: fmt.Errorf("update game data error"),
			},
			wantResult: nil,
			wantErr:       true,
		},
		{
			name: "should fails if" +
				" gameRepository PlayerSide return X" +
				" gameRepository GetAvailable return data" +
				" gameRepository UpdateGameData return nil",
			mocker: mocker{
				gameRepoPlayerSideResult:     &konst.PlayerXSide,
				gameRepoGetAvailableResult: []uint{1, 2, 3, 4, 5, 6, 7, 8, 9},
				gameRepoUpdateGameDataResult: nil,
			},
			wantResult: nil,
			wantErr:    true,
		},
		{
			name: "should fails if" +
				" gameRepository PlayerSide return X" +
				" gameRepository GetAvailable return data" +
				" gameRepository UpdateGameData return data" +
				" gameRepository GetGameScore return error",
			mocker: mocker{
				gameRepoPlayerSideResult:     &konst.PlayerXSide,
				gameRepoGetAvailableResult: []uint{9},
				gameRepoUpdateGameDataResult: [][]entities.PlayerSide{
					{"X", "O", "X"},
					{"X", "O", "O"},
					{"O", "X", "O"},
				},
				gameRepoGetGameScoreError: fmt.Errorf("GetGameScore error"),
			},
			wantResult:    nil,
			wantErr:       true,
		},
		{
			name: "should fails if" +
				" gameRepository PlayerSide return X" +
				" gameRepository GetAvailable return error" +
				" gameRepository UpdateGameData return data" +
				" gameRepository GetGameScore return data",
			mocker: mocker{
				gameRepoPlayerSideResult:  &konst.PlayerXSide,
				gameRepoUpdateGameDataResult: [][]entities.PlayerSide{
					{"X", "O", "X"},
					{"X", "O", "O"},
					{"O", "8", "9"},
				},
				gameRepoGetGameScoreResult: &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{},
						Columns:  map[uint]uint{},
						Diagonal: map[uint]uint{},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{},
						Columns:  map[uint]uint{},
						Diagonal: map[uint]uint{},
					},
				},
				gameRepoGetAvailableError: fmt.Errorf("GetAvailable error"),
			},
			wantResult: nil,
			wantErr:    true,
		},
		{
			name: "should got game end with the winner if" +
				" mameRepository PlayerSide return X" +
				" gameRepository UpdateGameData return data" +
				" gameRepository GetGameScore return return score at column 2 of player O match the game length",
			mocker: mocker{
				gameRepoPlayerSideResult:  &konst.PlayerXSide,
				gameRepoUpdateGameDataResult: [][]entities.PlayerSide{
					{"X", "O", "O"},
					{"O", "X", "O"},
					{"X", "8", "X"},
				},
				gameRepoGetGameScoreResult: &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 2, 1: 2, 2: 0},
						Columns:  map[uint]uint{0: 1, 1: 1, 3: 2},
						Diagonal: map[uint]uint{0: 0, 1: 1},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 0, 1: 2, 2: 2},
						Columns:  map[uint]uint{0: 2, 1: 2, 2: 3},
						Diagonal: map[uint]uint{0: 0, 1: 3},
					},
				},
				gameRepoGetAvailableResult: []uint{8},
			},
			wantResult: &entities.PlayGameResult{
				GameData:   [][]entities.PlayerSide{
					{"X", "O", "O"},
					{"O", "X", "O"},
					{"X", "8", "X"},
				},
				GameScore:  &entities.GameScore{
					PlayerX: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 2, 1: 2, 2: 0},
						Columns:  map[uint]uint{0: 1, 1: 1, 3: 2},
						Diagonal: map[uint]uint{0: 0, 1: 1},
					},
					PlayerO: entities.PlayerGameScore{
						Rows:     map[uint]uint{0: 0, 1: 2, 2: 2},
						Columns:  map[uint]uint{0: 2, 1: 2, 2: 3},
						Diagonal: map[uint]uint{0: 0, 1: 3},
					},
				},
				GameWinner: &konst.PlayerOSide,
				IsGameEnd:  &gameEnd,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gameRepo := new(mocks.GameRepositories)
			gameRepo.On("GetPlayerSide", mock.Anything).
				Return(tt.mocker.gameRepoPlayerSideResult, tt.mocker.gameRepoPlayerSideError)
			gameRepo.On("GetAvailable").
				Return(tt.mocker.gameRepoGetAvailableResult, tt.mocker.gameRepoGetAvailableError)
			gameRepo.On("UpdateGame", mock.Anything, mock.Anything).
				Return(tt.mocker.gameRepoUpdateGameDataResult, tt.mocker.gameRepoUpdateGameDataError)
			gameRepo.On("GetGameScore").
				Return(tt.mocker.gameRepoGetGameScoreResult, tt.mocker.gameRepoGetGameScoreError)
			g := NewGameUsecase(gameRepo)
			got, err := g.ComputerPlayGame()
			if (err != nil) != tt.wantErr {
				t.Errorf("ComputerPlayGame() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.wantResult) {
				t.Errorf("ComputerPlayGame() got = %v, want %v", got, tt.wantResult)
			}
		})
	}
}