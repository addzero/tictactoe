package game

import (
	"gitlab.com/orgeozerass/tictactoe/internal/entities"
	"gitlab.com/orgeozerass/tictactoe/internal/konst"
	"reflect"
	"testing"
)

func Test_gameRepository_UpdateGame(t *testing.T) {
	type args struct {
		i    uint
		side entities.PlayerSide
	}
	tests := []struct {
		name    string
		args    args
		setup   func()
		want    entities.GameData
		wantErr bool
	}{
		{
			name: "should fails if game is nil",
			args: args{
				i:    1,
				side: konst.PlayerXSide,
			},
			setup: func() {
				GetInstance().newGame(nil)
			},
			wantErr: true,
			want:    nil,
		},
		{
			name: "should fails if update game with specific tile number as not empty tile",
			args: args{
				i:    1,
				side: konst.PlayerXSide,
			},
			setup: func() {
				GetInstance().newGame([][]entities.PlayerSide{
					{"X", "2", "3"},
					{"4", "5", "6"},
					{"7", "8", "9"},
				})
			},
			wantErr: true,
			want:    nil,
		},
		{
			name: "should get update game data if update game with specific tile number empty tile",
			args: args{
				i:    3,
				side: konst.PlayerXSide,
			},
			setup: func() {
				GetInstance().newGame([][]entities.PlayerSide{
					{"X", "O", "3"},
					{"4", "5", "6"},
					{"7", "8", "9"},
				})
			},
			wantErr: false,
			want: [][]entities.PlayerSide{
				{"X", "O", "X"},
				{"4", "5", "6"},
				{"7", "8", "9"},
			},
		},
		{
			name: "should get update game data if update game with specific tile number empty tile",
			args: args{
				i:    9,
				side: konst.PlayerOSide,
			},
			setup: func() {
				GetInstance().newGame([][]entities.PlayerSide{
					{"X", "O", "X"},
					{"X", "O", "O"},
					{"O", "X", "9"},
				})
			},
			wantErr: false,
			want: [][]entities.PlayerSide{
				{"X", "O", "X"},
				{"X", "O", "O"},
				{"O", "X", "O"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			g := gameRepository{}
			got, err := g.UpdateGame(tt.args.i, tt.args.side)
			if (err != nil) != tt.wantErr {
				t.Errorf("UpdateGame() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UpdateGame() got = %v, want %v", got, tt.want)
			}
		})
	}
}
