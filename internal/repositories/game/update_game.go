package game

import (
	"fmt"
	"gitlab.com/orgeozerass/tictactoe/internal/entities"
)

func (g *gameRepository) UpdateGame(i uint, side entities.PlayerSide) (entities.GameData, error) {
	if GetInstance().getGame() == nil {
		return nil, fmt.Errorf("game not yet init")
	}
	isEmpty, avaiErr := g.IsAvailable(i)
	if avaiErr != nil {
		return nil, avaiErr
	}
	if !isEmpty {
		return nil, fmt.Errorf("specific tile is not empty")
	}

	GetInstance().updateGame(i, side)
	return GetInstance().getGame(), nil
}
