package game

import (
	"fmt"
	"gitlab.com/orgeozerass/tictactoe/internal/entities"
)

func (g *gameRepository) GetGameScore() (*entities.GameScore, error) {
	gameScore := GetInstance().getGameScore()
	if gameScore == nil {
		return nil, fmt.Errorf("game score is nil")
	}
	return gameScore, nil
}
