package game

import (
	"fmt"
	"gitlab.com/orgeozerass/tictactoe/internal/konst"
)

func (g *gameRepository) GetAvailable() ([]uint, error) {
	game := GetInstance().getGame()
	if game == nil {
		return nil, fmt.Errorf("game not yet init")
	}

	var available []uint
	for r := 0; r < len(game); r++ {
		for c := 0; c < len(game[r]); c++ {
			currentTile := game[r][c]
			if currentTile != konst.PlayerXSide && currentTile != konst.PlayerOSide {
				tileNumber := r * len(game) + c + 1
				available = append(available, uint(tileNumber))
			}
		}
	}
	return available, nil
}
