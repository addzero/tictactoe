package game

import (
	"fmt"
	"gitlab.com/orgeozerass/tictactoe/internal/entities"
)

func (g *gameRepository) GetPlayerSide() (*entities.PlayerSide, error) {
	playerSide := GetInstance().getPlayerSide()
	if playerSide == nil {
		return nil, fmt.Errorf("player side not yet selected")
	}
	return playerSide, nil
}
