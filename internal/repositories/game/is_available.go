package game

import (
	"fmt"
	"gitlab.com/orgeozerass/tictactoe/internal/konst"
	"gitlab.com/orgeozerass/tictactoe/internal/utils"
)

func (g *gameRepository) IsAvailable(i uint) (bool, error) {
	game := GetInstance().getGame()
	if game == nil {
		return false, fmt.Errorf("game not yet init")
	}

	row, column := utils.GetTileRowColumn(i, game)
	size := uint(len(game))
	if row > size - 1  || column > size - 1 {
		return false, nil
	}
	tileMarked := game[row][column]
	if tileMarked != konst.PlayerXSide && tileMarked != konst.PlayerOSide {
		return true, nil
	}
	
	return false, nil
}
