package game

import (
	"gitlab.com/orgeozerass/tictactoe/internal/entities"
	"gitlab.com/orgeozerass/tictactoe/internal/konst"
	"gitlab.com/orgeozerass/tictactoe/internal/repositories"
	"gitlab.com/orgeozerass/tictactoe/internal/utils"
	"sync"
)

type gameInstance interface {
	getGame() entities.GameData
	newGame(entities.GameData)
	setPlayerPlayFirst(bool)
	updateGame(uint, entities.PlayerSide)
	getPlayerSide() *entities.PlayerSide
	getGameScore() *entities.GameScore
	setGameScore(*entities.GameScore)
}

type gameMutex struct {
	player1Side       entities.PlayerSide
	player2Side       entities.PlayerSide
	isPlayerPlayFirst *bool
	game              [][]entities.PlayerSide
	gameScore         *entities.GameScore
	sync.RWMutex
}

func (g *gameMutex) setGameScore(score *entities.GameScore) {
	defer g.Unlock()
	g.Lock()
	g.gameScore = score
}

func (g *gameMutex) getGameScore() *entities.GameScore {
	defer g.Unlock()
	g.Lock()
	return g.gameScore
}

func (g *gameMutex) getPlayerSide() *entities.PlayerSide {
	if g.isPlayerPlayFirst == nil {
		return nil
	}
	if *g.isPlayerPlayFirst {
		return &g.player1Side
	}

	return &g.player2Side
}

func (g *gameMutex) setPlayerPlayFirst(isPlayer1 bool) {
	defer g.Unlock()
	g.Lock()
	g.isPlayerPlayFirst = &isPlayer1
}

func isDiagonal0(row, column uint) bool {
	return row == column
}

func isDiagonal1(row, column, size uint) bool {
	return row + column + 1 == size
}

func (g *gameMutex) updateGame(i uint, side entities.PlayerSide) {
	defer g.Unlock()
	g.Lock()
	game := g.game
	row, column := utils.GetTileRowColumn(i, game)
	isDiagonal0 := isDiagonal0(row, column)
	isDiagonal1 := isDiagonal1(row, column, uint(len(game)))
	game[row][column] = side
	switch side {
	case konst.PlayerXSide:
		g.gameScore.PlayerX.Rows[row] += 1
		g.gameScore.PlayerX.Columns[column] += 1
		if isDiagonal0 {
			g.gameScore.PlayerX.Diagonal[0] += 1
		}

		if isDiagonal1 {
			g.gameScore.PlayerX.Diagonal[1] += 1
		}
	case konst.PlayerOSide:
		g.gameScore.PlayerO.Rows[row] += 1
		g.gameScore.PlayerO.Columns[column] += 1
		if isDiagonal0 {
			g.gameScore.PlayerO.Diagonal[0] += 1
		}

		if isDiagonal1 {
			g.gameScore.PlayerO.Diagonal[1] += 1
		}
	}
	return
}

var game *gameMutex

func (g *gameMutex) getGame() entities.GameData {
	defer g.Unlock()
	g.Lock()
	return g.game
}

func (g *gameMutex) newGame(newGame entities.GameData) {
	defer g.Unlock()
	g.Lock()
	g.game = newGame
	g.gameScore = &entities.GameScore{
		PlayerX: entities.PlayerGameScore{
			Rows: map[uint]uint{},
			Columns: map[uint]uint{},
			Diagonal: map[uint]uint{},
		},
		PlayerO: entities.PlayerGameScore{
			Rows: map[uint]uint{},
			Columns: map[uint]uint{},
			Diagonal: map[uint]uint{},
		},
	}
	return
}

func GetInstance() gameInstance {
	if game == nil {
		game = &gameMutex{
			RWMutex:     sync.RWMutex{},
			player1Side: konst.PlayerXSide,
			player2Side: konst.PlayerOSide,
		}
	}
	return game
}

type gameRepository struct {
}

func NewGameRepository() repositories.GameRepositories {
	return &gameRepository{}
}
