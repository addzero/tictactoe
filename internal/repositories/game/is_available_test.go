package game

import (
	"gitlab.com/orgeozerass/tictactoe/internal/entities"
	"testing"
)

func Test_gameRepository_IsAvailable(t *testing.T) {
	type args struct {
		i uint
	}
	tests := []struct {
		name    string
		args    args
		setup   func()
		want    bool
		wantErr bool
	}{
		{
			name:    "should return error if game is empty",
			args:    args{i: 1},
			setup:   func() {
				GetInstance().newGame(nil)
			},
			wantErr: true,
			want:    false,
		},
		{
			name: "should return false if specific tile is 0",
			args: args{i: 0},
			setup: func() {
				GetInstance().newGame([][]entities.PlayerSide{
					{"X", "O", "3"},
					{"4", "5", "6"},
					{"7", "8", "9"},
				})
			},
			want: false,
		},
		{
			name: "should return false if specific tile is greater than game max tile number",
			args: args{i: 10},
			setup: func() {
				GetInstance().newGame([][]entities.PlayerSide{
					{"X", "O", "3"},
					{"4", "5", "6"},
					{"7", "8", "9"},
				})
			},
			want: false,
		},
		{
			name: "should return false if specific tile is not empty",
			args: args{i: 1},
			setup: func() {
				GetInstance().newGame([][]entities.PlayerSide{
					{"X", "O", "3"},
					{"4", "5", "6"},
					{"7", "8", "9"},
				})
			},
			want: false,
		},
		{
			name: "should return false if specific tile is empty (1 tile marked)",
			args: args{i: 1},
			setup: func() {
				GetInstance().newGame([][]entities.PlayerSide{
					{"1", "O", "3"},
					{"4", "5", "6"},
					{"7", "8", "9"},
				})
			},
			want: true,
		},
		{
			name: "should return false if specific tile is empty 8 tiles marked",
			args: args{i: 9},
			setup: func() {
				GetInstance().newGame([][]entities.PlayerSide{
					{"X", "O", "O"},
					{"O", "O", "X"},
					{"X", "X", "9"},
				})
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			g := gameRepository{}
			tt.setup()
			got, err := g.IsAvailable(tt.args.i)
			if (err != nil) != tt.wantErr {
				t.Errorf("IsAvailable() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("IsAvailable() got = %v, want %v", got, tt.want)
			}
		})
	}
}
