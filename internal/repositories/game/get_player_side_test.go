package game

import (
	"gitlab.com/orgeozerass/tictactoe/internal/entities"
	"gitlab.com/orgeozerass/tictactoe/internal/konst"
	"reflect"
	"testing"
)

func Test_gameRepository_GetPlayerSide(t *testing.T) {
	tests := []struct {
		name    string
		setup   func()
		want    *entities.PlayerSide
		wantErr bool
	}{
		{
			name:    "should get error if not yet set player is play first",
			setup:   func() {},
			wantErr: true,
		},
		{
			name: "should get player side as X if set player play first as true",
			setup: func() {
				GetInstance().setPlayerPlayFirst(true)
			},
			wantErr: false,
			want:    &konst.PlayerXSide,
		},
		{
			name: "should get player side as X if set player play first as false",
			setup: func() {
				GetInstance().setPlayerPlayFirst(false)
			},
			wantErr: false,
			want:    &konst.PlayerOSide,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			g := gameRepository{}
			got, err := g.GetPlayerSide()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetPlayerSide() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetPlayerSide() got = %v, want %v", got, tt.want)
			}
		})
	}
}
