package game

import (
	"gitlab.com/orgeozerass/tictactoe/internal/entities"
	"reflect"
	"testing"
)

func Test_gameRepository_GetAvailable(t *testing.T) {
	tests := []struct {
		name    string
		setup   func()
		want    []uint
		wantErr bool
	}{
		{
			name: "should get error if game not yet set",
			setup: func() {
				GetInstance().newGame(nil)
			},
			want: nil,
			wantErr: true,
		},
		{
			name: "should get available as expect 1",
			setup: func() {
				GetInstance().newGame([][]entities.PlayerSide{
					{"X", "O", "3"},
					{"4", "5", "6"},
					{"7", "8", "9"},
				})
			},
			want: []uint{3, 4, 5, 6, 7, 8, 9},
			wantErr: false,
		},
		{
			name: "should get available as expect 2",
			setup: func() {
				GetInstance().newGame([][]entities.PlayerSide{
					{"X", "O", "3"},
					{"4", "5", "6"},
					{"7", "X", "9"},
				})
			},
			want: []uint{3, 4, 5, 6, 7, 9},
			wantErr: false,
		},
		{
			name: "should get available as expect 3",
			setup: func() {
				GetInstance().newGame([][]entities.PlayerSide{
					{"X", "O", "X"},
					{"O", "X", "O"},
					{"O", "X", "O"},
				})
			},
			want: nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			g := &gameRepository{}
			got, err := g.GetAvailable()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetAvailable() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetAvailable() got = %v, want %v", got, tt.want)
			}
		})
	}
}