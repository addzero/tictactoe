package game

import (
	"fmt"
	"gitlab.com/orgeozerass/tictactoe/internal/entities"
	"gitlab.com/orgeozerass/tictactoe/internal/validator"
)

func (g *gameRepository) CreateNewGame(param entities.GameStartParam) (entities.GameData, error) {
	valPlayerSideErr := validator.ValidateVar(param.PlayerSide, "required,oneof='X' 'O'")
	if valPlayerSideErr != nil {
		return nil, valPlayerSideErr
	}

	valGameSizeErr := validator.ValidateVar(param.GameSize, "required,min=3")
	if valGameSizeErr != nil {
		return nil, valGameSizeErr
	}

	var col [][]entities.PlayerSide
	for r := 0; r < int(param.GameSize); r++ {
		var row []entities.PlayerSide
		for c := 1; c <= int(param.GameSize); c++ {
			tileNum := (uint(c) % (param.GameSize + 1)) + uint(r)*param.GameSize
			tileNumber := fmt.Sprintf("%d", tileNum)
			row = append(row, entities.PlayerSide(tileNumber))
		}
		col = append(col, row)
	}

	GetInstance().newGame(col)
	return col, nil
}
