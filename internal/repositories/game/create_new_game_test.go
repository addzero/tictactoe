package game

import (
	"gitlab.com/orgeozerass/tictactoe/internal/entities"
	"gitlab.com/orgeozerass/tictactoe/internal/konst"
	"reflect"
	"testing"
)

func Test_gameRepository_CreateNewGame(t *testing.T) {
	type args struct {
		param entities.GameStartParam
	}
	tests := []struct {
		name    string
		args    args
		want    entities.GameData
		wantErr bool
	}{
		{
			name: "should return error if player side is empty",
			args: args{param: entities.GameStartParam{
				GameSize:   3,
				PlayerSide: "",
			}},
			wantErr: true,
		},
		{
			name: "should return error if player side is not X or O (A)",
			args: args{param: entities.GameStartParam{
				GameSize:   3,
				PlayerSide: entities.PlayerSide("A"),
			}},
			wantErr: true,
		},
		{
			name: "should return error if player side is not X or O (B)",
			args: args{param: entities.GameStartParam{
				GameSize:   3,
				PlayerSide: entities.PlayerSide("B"),
			}},
			wantErr: true,
		},
		{
			name: "should return error if player side is not X or O (Z)",
			args: args{param: entities.GameStartParam{
				GameSize:   3,
				PlayerSide: entities.PlayerSide("Z"),
			}},
			wantErr: true,
		},
		{
			name: "should return error if game size smaller than 3 (2)",
			args: args{param: entities.GameStartParam{
				GameSize:   2,
				PlayerSide: konst.PlayerXSide,
			}},
			wantErr: true,
		},
		{
			name: "should return error if game size smaller than 3 (1)",
			args: args{param: entities.GameStartParam{
				GameSize:   1,
				PlayerSide: konst.PlayerXSide,
			}},
			wantErr: true,
		},
		{
			name: "should return game data for game size 3",
			args: args{param: entities.GameStartParam{
				GameSize:   3,
				PlayerSide: konst.PlayerXSide,
			}},
			wantErr: false,
			want: [][]entities.PlayerSide{
				{"1", "2", "3"},
				{"4", "5", "6"},
				{"7", "8", "9"},
			},
		},
		{
			name: "should return game data for game size 4",
			args: args{param: entities.GameStartParam{
				GameSize:   4,
				PlayerSide: konst.PlayerXSide,
			}},
			wantErr: false,
			want: [][]entities.PlayerSide{
				{"1", "2", "3", "4"},
				{"5", "6", "7", "8"},
				{"9", "10", "11", "12"},
				{"13", "14", "15", "16"},
			},
		},
		{
			name: "should return game data for game size 5",
			args: args{param: entities.GameStartParam{
				GameSize:   5,
				PlayerSide: konst.PlayerXSide,
			}},
			wantErr: false,
			want: [][]entities.PlayerSide{
				{"1", "2", "3", "4", "5"},
				{"6", "7", "8", "9", "10"},
				{"11", "12", "13", "14", "15"},
				{"16", "17", "18", "19", "20"},
				{"21", "22", "23", "24", "25"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			g := gameRepository{}
			got, err := g.CreateNewGame(tt.args.param)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateNewGame() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateNewGame() got = %v, want %v", got, tt.want)
			}
		})
	}
}
