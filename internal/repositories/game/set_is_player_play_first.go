package game

func (g *gameRepository) SetIsPlayerPlayFirst(isPlayFirst bool) error {
	GetInstance().setPlayerPlayFirst(isPlayFirst)
	return nil
}
