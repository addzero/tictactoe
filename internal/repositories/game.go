package repositories

import "gitlab.com/orgeozerass/tictactoe/internal/entities"

type GameRepositories interface {
	CreateNewGame(param entities.GameStartParam) (entities.GameData, error)
	GetPlayerSide() (*entities.PlayerSide, error)
	UpdateGame(uint, entities.PlayerSide) (entities.GameData, error)
	GetAvailable() ([]uint, error)
	IsAvailable(uint) (bool, error)
	SetIsPlayerPlayFirst(bool) error
	GetGameScore() (*entities.GameScore, error)
}
